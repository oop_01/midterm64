package com.manita.midterm;

public class Bmi {
    private String name;
    private double weight;
    private double height;
    private String editname;
    private double editweight;
    private double editheight;

    public Bmi(String name, double weight, double height) {
        this.name = name;
        this.weight = weight;
        this.height = height;
    }

    public void print() {
        System.out.println("Name: " + name);
        System.out.println("Weight: " + weight);
        System.out.println("Height: " + height);
        printBmi();
    }

    public void editName(String editname) {
        name = editname;
    }

    public void editWeight(double editweight) {
        weight = editweight;
    }

    public void editHeight(double editheight) {
        height = editheight;
    }

    public void editAll(String editname, double editweight, double editheight) {
        name = editname;
        weight = editweight;
        height = editheight;
    }
    public double bmiCalculator() {
        double meter;
        meter = height / 100;
        return weight / Math.pow(meter, 2);
    }

    public void printBmi() {
        System.out.println("BMI = " + bmiCalculator());
        criterion();
    }
    public void criterion() {
        if (bmiCalculator() < 18.5) {
            System.out.println("Too skinny, you should take more care of yourself.");
        }
        if (bmiCalculator() > 18.5 && bmiCalculator() <= 24) {
            System.out.println("Normal weight, try to keep it level!!!");
        }
        if (bmiCalculator() > 25 && bmiCalculator() <= 29.9) {
            System.out.println("start to plump, You can start taking care of yourself.!!!");
        }
        if (bmiCalculator() >= 30) {
            System.out.println("Overweight, please take care of yourself ;)");
        }
    }
    public String getName() {
        return name;
    }
    public double getWeight() {
        return weight;
    }
    public double getHeight() {
        return height;
    }
    public String getEditName() {
        return editname ;
    }
    public double getEditWeight() {
        return editweight;
    }
    public double getEditHeight() {
        return editheight;
    }

}
