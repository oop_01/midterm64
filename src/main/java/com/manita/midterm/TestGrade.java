package com.manita.midterm;

public class TestGrade {
    public static void main(String[] args) {
        Grade std1 = new Grade("Rose", 16450, 78);
        std1.show();
        std1.editScore(80);
        std1.show();

        System.out.println();
        Grade std2 = new Grade("Iris", 16404, 63);
        std2.show();

        System.out.println();
        Grade std3 = new Grade("Lena", 16310, 32);
        std3.show();
        std3.editScore(52);
        std3.show();

    }
}
