package com.manita.midterm;

public class Grade {
    private String name;
    private int id;
    private int score;
    private String editname;
    private int editid;
    private int editscore;

    public Grade(String name, int id, int score) {
        this.name = name;
        this.id = id;
        this.score = score;
    }

    public void editName(String editname) {
        name = editname;
    }

    public void editId(int editid) {
        id = editid;
    }

    public void editScore(int editscore) {
        score = editscore;
    }

    public void editAll(String editname, int id, int editscore) {
        name = editname;
        id = editid;
        score = editscore;
    }

    public void show() {
        System.out.println("Name : " + name);
        System.out.println("ID : " + id);
        System.out.println("Score : " + score);
        checkGrade();
    }

    public void checkGrade() {
        if (score >= 80) {
            System.out.println("your grade is A");
        }
        if (score >= 75 && score <= 79) {
            System.out.println("your grade is B+");
        }
        if (score >= 70 && score <= 74) {
            System.out.println("your grade is B");
        }
        if (score >= 65 && score <= 69) {
            System.out.println("your grade is C+");
        }
        if (score >= 60 && score <= 64) {
            System.out.println("your grade is C");
        }
        if (score >= 55 && score <= 59) {
            System.out.println("your grade is D+");
        }
        if (score >= 50 && score <= 54) {
            System.out.println("your grade is D");
        }
        if (score <= 49) {
            System.out.println("your grade is F");
        }
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getScore() {
        return score;
    }

    public String getEditName() {
        return editname;
    }

    public int getEditId() {
        return editid;
    }

    public int getEditScore() {
        return editscore;
    }

}
