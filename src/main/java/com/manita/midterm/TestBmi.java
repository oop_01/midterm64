package com.manita.midterm;

public class TestBmi {
    public static void main(String[] args) {
        Bmi jane = new Bmi("Jane", 42, 162);
        jane.print();
        jane.editHeight(160);
        jane.print();
        
        System.out.println();
        Bmi lily = new Bmi("Lily", 50, 163);
        lily.print();

        System.out.println();
        Bmi eve = new Bmi("Eve", 74, 162);
        eve.print();
        eve.editWeight(80);
        eve.print();

    
    }
    
}
